export function sum(...args:number[]){
    return args.reduce((a,b)=>a+b);
}

export function mul(multiply:number,...args:number[]):number[]{
    return args.map((a)=>a*multiply);
}
