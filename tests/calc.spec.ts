import {expect} from "chai";
import {sum,mul} from "../src/calc";

describe("the calc module", ()=>{
    context("#sum",()=>{
        it("should be a function",()=>{
            expect(sum).to.be.a("function");
            expect(sum).to.be.instanceOf(Function);
        });

        it("should calculate sum of 2 numbers",()=>{
            const res = sum(3,5);
            expect(res).to.be.equal(8);
        });

        it("should calculate sum of several numbers",()=>{
            const res = sum(3,5,7,9,10);
            expect(res).to.be.equal(34);
        });
    });

    context("#Mult",()=>{
        it("should be a function",()=>{
            expect(mul).to.be.a("function");
            expect(mul).to.be.instanceOf(Function);
        });

        it("should calculate sum of 2 numbers",()=>{
            const res = mul(3,5);
            expect(res).to.deep.equal([15]);
        });

        it("should calculate sum of several numbers",async ()=>{
            const res = mul(3,5,2,4,10);
            expect(res).to.deep.equal([15,6,12,30]);
        });
    });


});